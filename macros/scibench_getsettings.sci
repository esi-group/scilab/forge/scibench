// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function env = scibench_getsettings (  )
    // Inquire the parameters of Scilab and its environment.
    //
    // Calling Sequence
    //   env = scibench_getsettings (  )
    //
    // Parameters
    //   env : a tlist with fields
    //   env.nproc : a 1x1 matrix of floating point integers, the number of processors
    //   env.os : a 1x1 matrix of strings, the operating system
    //   env.archi : a 1x1 matrix of strings, the 32/64 bits architecture
    //   env.cpu : a 1x1 matrix of strings, the type of CPU
    //   env.version : a 1x1 matrix of strings, the version of Scilab
    //
    // Description
    //   This function allows to gather major informations related to the
    //   environment where the script is launched.
    //
    // Examples
    // env = scibench_getsettings ( )
    //
    // Authors
    //   2010 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "scibench_getsettings" , rhs , 0 )
    apifun_checklhs ( "scibench_getsettings" , lhs , 0:1 )
    //
    env = tlist(["BENCHENV" "nproc" "os" "archi" "cpu" "version"])
    env.nproc = ""
    env.os = ""
    env.archi = ""
    env.version = ""
    env.nproc = 0
    env.cpu = ""
    //
    // Get Scilab version
    [version,opts] = getversion();
    env.version = version
    //
    // Get OS
    env.os = getos()
    //
    // Get architecture
    env.archi = opts(2);
    OS=getos()
    if ( OS == "Windows" ) then
        //
        // Get Number of procs
        a=getdebuginfo();
        k=grep(a,"Number of processors");
        if ( k <> [] ) then
            anproc=strsplit(a(k),":");
            env.nproc = evstr(anproc(2));
        end
        //
        // Get CPU
        k=grep(a,"Operating System");
        if ( k <> [] ) then
            env.cpu = a(k+1);
        end
    else
      if ( fileinfo("/proc/cpuinfo") <> [] ) then
      txt=mgetl("/proc/cpuinfo")
      //
      // Get Number of procs
      k=grep(txt,"/processor.*:/","r")
      if ( k <> [] ) then
        env.nproc = size(k,"*")
      end
      //
      // Get CPU (the first one is k(1))
      k=grep(txt,"/model name.*:/","r");
      if ( k <> [] ) then
        env.cpu = txt(k(1))
      end
      end
    end
endfunction


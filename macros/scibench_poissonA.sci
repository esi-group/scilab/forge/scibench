// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - Neeraj Sharma and Matthias K. Gobbert

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = scibench_poissonA ( N )
    // Returns a sparse matrix for 2D Poisson PDE.
    //
    // Calling Sequence
    //   A = scibench_poissonA ( N )
    //
    // Parameters
    //   N : a 1-by-1 matrix of doubles, integer values. The number of cells.
    //   A : a m-by-m matrix of doubles, the sparse matrix where m = N^2.
    //
    // Description
    // We compute the numerical solution with finite
    // differences for the Poisson problem with homogeneous
    // Dirichlet boundary conditions.
    //
    // We consider the 2 dimensional problem Partial Differential Equation:
    //
    // <latex>
    // \begin{array}{rlll}
    // - \Delta u &=& f &\textrm{ in the domain,} \\
    //          u &=& 0 &\textrm{ on the frontier.}
    // \end{array}
    // </latex>
    //
    // where the Laplace operator is
    //
    // <latex>
    // \Delta u = \frac{\partial^2 u}{dx^2} + \frac{\partial^2 u}{dy^2}
    // </latex>
    //
    // We consider the domain 0 <= x1 <= 1, 0 <= x2 <= 1.
    // The function f is defined by
    // f(x, y) = -2pi^2 cos(2pix) sin^2(piy) - 2pi^2 sin^2(pix) cos(2piy).
    // The solution is u(x, y) = sin^2(pix) sin^2(piy).
    // We use a grid of N-by-N points.
    // We use a second order finite difference approximation of the Laplace
    // operator.
    //
    // The Kronecker operator is used, so that the computation is vectorized.
    //
    // Examples
    // stacksize("max");
    // A = scibench_poissonA(50);
    // PlotSparse(A)
    //
    // Bibliography
    // "A Comparative Evaluation Of Matlab, Octave, Freemat, And Scilab For Research And Teaching", 2010, Neeraj Sharma and Matthias K. Gobbert

        s = [-1*ones(1,N-1) 2*ones(1,N) -1*ones(1,N-1)]'
        i = [2:N 1:N 1:N-1]'
        j = [1:N-1 1:N 2:N ]';
        ij = [i j]
        T = sparse(ij,s)
        I = speye(N,N);
        A = I .*. T + T .*. I;
endfunction


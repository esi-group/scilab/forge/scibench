// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.





//
// %BENCHENV_string --
//   Returns the string containing the BENCHENV
//
function str = %BENCHENV_string ( this )
  str = []
  k = 1
  str(k) = msprintf("Benchmark Environment:")
  k = k + 1
  str(k) = msprintf("=========================")
  k = k + 1
  str(k) = msprintf("version: %s\n", this.version)
  k = k + 1
  str(k) = msprintf("nproc: %s\n", _tostring(this.nproc))
  k = k + 1
  str(k) = msprintf("os: %s\n", this.os)
  k = k + 1
  str(k) = msprintf("archi: %s\n", this.archi)
  k = k + 1
  str(k) = msprintf("cpu: %s\n", this.cpu)
endfunction

function s = _tostring ( x )
  if ( x==[] ) then
    s = "[]"
  else
    n = size ( x , "*" )
    if ( n == 1 ) then
      s = string(x)
    else
      s = "["+strcat(string(x)," ")+"]"
    end
  end
endfunction



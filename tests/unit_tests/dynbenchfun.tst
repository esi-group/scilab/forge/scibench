// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// Do not check ref, because timings cannot be reproduced exactly.
// <-- NO CHECK REF -->
//

/////////////////////////////////////////////////////////////
function t = pascalup_col (n)
   // Pascal up matrix.
   // Column by column version
   tic()
   c = eye(n,n)
   c(1,:) = ones(1,n)
   for i = 2:(n-1)
      c(2:i,i+1) = c(1:(i-1),i)+c(2:i,i)
   end
   t = toc()
endfunction

stacksize("max");

perftable = scibench_dynbenchfun ( pascalup_col );
assert_checkequal ( typeof(perftable) , "constant" );
assert_checkequal ( and(perftable(:,1)>=1) , %t );
assert_checkequal ( and(perftable(:,2)>=0) , %t );
//
perftable = scibench_dynbenchfun ( pascalup_col , %f );
assert_checkequal ( typeof(perftable) , "constant" );
assert_checkequal ( and(perftable(:,1)>=1) , %t );
assert_checkequal ( and(perftable(:,2)>=0) , %t );
//
perftable = scibench_dynbenchfun ( pascalup_col , %f , %f );
assert_checkequal ( typeof(perftable) , "constant" );
assert_checkequal ( and(perftable(:,1)>=1) , %t );
assert_checkequal ( and(perftable(:,2)>=0) , %t );
//
perftable = scibench_dynbenchfun ( pascalup_col , %f , %f , 0.001 );
assert_checkequal ( typeof(perftable) , "constant" );
assert_checkequal ( and(perftable(:,1)>=1) , %t );
assert_checkequal ( and(perftable(:,2)>=0) , %t );
//
perftable = scibench_dynbenchfun ( pascalup_col , %f , %f , 0.001 , 0.1 );
assert_checkequal ( typeof(perftable) , "constant" );
assert_checkequal ( and(perftable(:,1)>=1) , %t );
assert_checkequal ( and(perftable(:,2)>=0) , %t );
//
perftable = scibench_dynbenchfun ( pascalup_col , %f , %f , 0.001 , 0.1 , 1.1 );
assert_checkequal ( typeof(perftable) , "constant" );
assert_checkequal ( and(perftable(:,1)>=1) , %t );
assert_checkequal ( and(perftable(:,2)>=0) , %t );


// With an additionnal input argument
function t = pascalup_col2 ( n , alpha )
   [lhs, rhs] = argn()
   if ( rhs <> 2 ) then
     error("Wrong number of input arguments")
   end
   tic()
   c = eye(n,n) * alpha
   c(1,:) = ones(1,n)
   for i = 2:(n-1)
      c(2:i,i+1) = c(1:(i-1),i)+c(2:i,i)
   end
   t = toc()
endfunction

scibench_dynbenchfun ( list(pascalup_col2,2) , %f , %f , [] , 0.1 );
//
// Use a callback to print the timings
function stop = myoutfun(k,n,t)
  global __counter__
  // Do nothing
  __counter__ = __counter__ + 1
  stop = %f
  //mprintf("Run #%d, n=%d, t=%.2f\n",k,n,t)
endfunction
global __counter__
__counter__ = 0;
scibench_dynbenchfun ( pascalup_col , %f , %f , [] , 0.1 , [] , myoutfun );
assert_checkequal ( __counter__>0 , %t );

//
// Use a callback to print the timings.
// The callback requires additionnal arguments.
function stop = myoutfun2(k,n,t,data)
  global __counter__
  // Do nothing
  __counter__ = __counter__ + 1
  stop = %f
  //mprintf("Run #%d, n=%d, t=%.2f\n",k,n,t)
endfunction
global __counter__
__counter__ = 0;
scibench_dynbenchfun ( pascalup_col , %f , %f , [] , 0.1 , [] , list(myoutfun2,2) );
assert_checkequal ( __counter__>0 , %t );

//
// Use a callback to print the timings.
// The callback stops the algorithm
function stop = myoutfun3(k,n,t)
  global __counter__
  // Do nothing
  __counter__ = __counter__ + 1
  stop = ( __counter__ >= 2 )
  //mprintf("Run #%d, n=%d, t=%.2f\n",k,n,t)
endfunction
global __counter__
__counter__ = 0;
scibench_dynbenchfun ( pascalup_col , %f , %f , [] , 0.1 , [] , myoutfun3 );
assert_checkequal ( __counter__ >=2 , %t );

//
// Use a callback to print the timings and stop the algorithm.
function stop = myoutfun4(k,n,t,kmax)
  global __counter__
  // Do nothing
  __counter__ = __counter__ + 1
  stop = ( k >= kmax )
  //mprintf("Run #%d, n=%d, t=%.2f\n",k,n,t)
endfunction
global __counter__
__counter__ = 0;
scibench_dynbenchfun ( pascalup_col , %f , %f , [] , 0.1 , [] , list(myoutfun4,2) );
assert_checkequal ( __counter__ >=2 , %t );



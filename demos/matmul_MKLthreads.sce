// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function n = matmul_searchforsize ( timemin , nfact )
    // Returns the size of the matrix for which the A*B product is larger than timemin
    //
    // Example
    // stacksize("max");
    // n = matmul_searchforsize ( 1 , 1.2 )

    //
    // Let the size of the matrix grow dynamically

    backrand = rand("info")
    rand( "normal" );

    //
    // Make a loop over n
    n = 1;
    k = 1;
    while ( %t )
        A = rand(n,n);
        B = rand(n,n);
        tic();
        ierr = execstr("C = A * B","errcatch");
        t = toc();
        if ( ierr <> 0 ) then
            laerr = lasterror();
            msg = sprintf(gettext("%s: Error while running benchmark: %s."),..
            "matmul_searchforsize",laerr);
            warning(msg)
            break
        end
        if ( t > timemin ) then
            break
        end
        n = ceil(nfact * n);
    end
    rand(backrand)
endfunction

function errmsg = specfun_checkflint ( funname , var , varname , ivar )
  // Checks that the argument is a floating point integer.
  //
  // Calling Sequence
  //   errmsg = specfun_checkflint ( var )
  //
  // Parameters
  //   funname : a 1 x 1 matrix of strings, the name of the calling function.
  //   var : a 1 x 1 matrix of doubles, the variable
  //   varname : a 1 x 1 matrix of string, the name of the variable
  //   ivar : a 1 x 1 matrix of floating point integers, the index of the input argument in the calling sequence
  //   errmsg : a 1 x 1 matrix of strings, the error message. If there was no error, the error message is the empty matrix.
  //
  // Description
  // This function is designed to be used to design functions where an
  // input argument is expected to be a floating point integer, that is, it has
  // the type "constant", but its content is an integer.
  // The error is generated if the condition or(floor(var)<>var) is true.
  //
  // Examples
  // // The function takes an argument x such that x>=1.
  // function y = myfunction ( x )
  //   specfun_checkflint ( "myfunction" , x , "x" , 1 )
  //   y = 2*x
  // endfunction
  // // Calling sequences which work
  // myfunction ( 2 )
  // // Calling sequences which generate an error
  // myfunction ( 1.1 )
  //
  // Authors
  // Michael Baudin - 2010
  //

  [lhsnb,rhsnb]=argn()
  if ( rhsnb <> 4 ) then
    msgfmt = "%s: Unexpected number of input arguments : %d provided while %d are expected."
    errmsg = msprintf(gettext(msgfmt), "specfun_checkflint", rhsnb,4)
    error(errmsg)
  end
  //
  // Checking type of input arguments
  if ( typeof(funname) <> "string" ) then
    errmsg = msprintf(gettext("%s: Expected a %s for input argument %s at input #%d, but got %s instead."), "specfun_checkflint", "string" , "funname" , 1 , funname , typeof(funname) );
    error(errmsg)
  end
  if ( typeof(var) <> "constant" ) then
    errmsg = msprintf(gettext("%s: Expected a %s for input argument %s at input #%d, but got [%s] instead."), "specfun_checkflint", "constant" , "var" , 2 , var , typeof(var) );
    error(errmsg)
  end
  if ( typeof(varname) <> "string" ) then
    errmsg = msprintf(gettext("%s: Expected a %s for input argument %s at input #%d, but got [%s] instead."), "specfun_checkflint", "string" , "varname" , 3 , varname , typeof(varname) );
    error(errmsg)
  end
  if ( typeof(ivar) <> "constant" ) then
    errmsg = msprintf(gettext("%s: Expected a %s for input argument %s at input #%d, but got [%s] instead."), "specfun_checkflint", "constant" , "ivar" , 4 , ivar , typeof(ivar) );
    error(errmsg)
  end
  //
  // Checking size of input arguments
  if ( or(size(funname) <> [1 1]) ) then
    strcomp = strcat(string(size(funname))," ")
    errmsg = msprintf(gettext("%s: Expected a scalar for input argument %s at input #%d, but got [%s] instead."), "specfun_checkflint", "funname" , 1 , strcomp );
    error(errmsg)
  end
  if ( or(size(varname) <> [1 1]) ) then
    strcomp = strcat(string(size(varname))," ")
    errmsg = msprintf(gettext("%s: Expected a scalar for input argument %s at input #%d, but got [%s] instead."), "specfun_checkflint", "varname" , 3 , varname );
    error(errmsg)
  end
  if ( or(size(ivar) <> [1 1]) ) then
    strcomp = strcat(string(size(ivar))," ")
    errmsg = msprintf(gettext("%s: Expected a scalar for input argument %s at input #%d, but got [%s] instead."), "specfun_checkflint", "ivar" , 4 , strcomp );
    error(errmsg)
  end
  //
  errmsg = []
  if ( or(floor(var)<>var) ) then
    k = find ( floor(var)<>var , 1 )
    errmsg = msprintf(gettext("%s: Wrong content of input argument %s at input #%d: expected a floating point integer, but entry #%d is equal to %s."),funname,varname,ivar,k,string(var(k)))
    error ( errmsg)
  end
endfunction


function [n,t,vart,S] = matmul_MKLthreads ( timemin , kmax , nfact , nthr )
    // Analyse performance of matmul w.r.t. MKL threads.
    //
    // Calling Sequence
    //   [n,t,mflops,vart,S] = matmul_MKLthreads ( timemin , kmax , nfact , nthr )
    //
    // Parameters
    //   timemin : a 1-by-1 matrix of doubles, the minimum time (seconds) for one run.
    //   kmax : a 1-by-1 matrix of floating point integers, the number of runs for each matrix size.
    //   nfact : a 1-by-1 matrix of doubles, must be greater than 1, the update factor of the matrix size computation.
    //   nthr : a 1-by-1 matrix of floating point integers, the maximum number of threads.
    //   n : a 1-by-1 matrix of floating point integers, the matrix size
    //   t : a nthr-by-kmax matrix of doubles, the execution times. The variable t(i,k) contains the times for i threads and the simulation #k, with i=1,2,..., nthr and k=1,2,...,kmax.
    //   vart : a nthr-by-1 matrix of doubles, the relative variability in time (in percent). vart(i) is the relative variability of the elapsed time with i threads, with i=1,2,...,nthr.
    //   S : a nthr-by-1 matrix of doubles, the speedup. The speedups are computed on the averaged times. The speedup S(i), for i in nthr is defined by S(i)=T(1)/T(i) where T(1) is the average time for 1 thread and T(i) is the average time for i threads.
    //
    // Description
    //   Analyse the sensitivity of the performance of matmul depending on the number of threads in the MKL.
    //
    // First, compute a matrix size n.
    // The matrix size is determined automatically from timemin and nfact.
    // Ideally, the matrix size n is the smallest n so that the time for one run is larger than timemin.
    // The matrix size n is updated from n = n * nfact.
    //
    // The main algorithm is the following.
    // Perform a loop on the number of threads, from 1 to nthr.
    // For a particular number of threads, perform kmax matrix multiply with n-by-n matrices.
    //
    // Example
    // scf();
    // [n,t,vart] = matmul_MKLthreads ( 0.5 , 30 , 1.2 , 1:4 );
    //
    // Authors
    // Copyright (C) 2010 - DIGITEO - Michael Baudin


    function C = mymatmul ( A , B )
        C = A * B;
    endfunction

    backnumthreads = MKL_Get_Max_Threads()
    //
    // Left plot: the variability
    subplot(1,2,1)
    thetitle = msprintf("Performance Variability of Matrix-Matrix Product.")
    xtitle(thetitle,"MKL Threads","Time (s)")
    //
    // Right plot: the speedup
    subplot(1,2,2)
    xtitle("Speedup","MKL Threads","Average Speedup")
    //
    // Compute the matrix size and update the title
    subplot(1,2,1)
    mprintf("Searching for matrix size...\n")
    n = matmul_searchforsize ( timemin , nfact )
    mprintf("Matrix size n=%d\n",n)
    thetitle = msprintf("Performance Variability of Matrix-Matrix Product: n=%d.",n)
    xtitle(thetitle,"MKL Threads","Time (s)")
    //
    // Fill the left plot
    subplot(1,2,1)
    A = rand(n,n);
    B = rand(n,n);
    for i = 1 : nthr
        mprintf("Running with %d threads...\n",i)
        MKL_Set_Num_Threads(i);
        [ti,msg] = scibench_benchfun ( mymatmul , "mymatmul" , list(A,B) , 0 , kmax , %f )
        Tmin = min(ti)
        Tmax = max(ti)
        vart(i) = (Tmax-Tmin)/max([Tmax %eps]) * 100
        t(i,1:kmax) = ti(:)'
        plot(ones(kmax,1)*i,ti,"bo")
        mprintf("Threads: %d, Tmin=%.2f, Tmax=%.2f, Relative variability: %.2f %%\n",i,Tmin,Tmax,vart(i))
    end
    //
    // Plot min, max and average
    plot(nthr,min(t,"c"),"r-")
    plot(nthr,max(t,"c"),"b--")
    plot(nthr,mean(t,"c"),"k-.")
    //
    // Compute and plot the speedup.
    T1=mean(t(1,:));
    S=ones(1,nthr);
    for i = 2:nthr
        S(i) = T1/mean(t(i,:));
    end
    //
    // Fill the speedup plot
    subplot(1,2,2)
    plot((1:nmax),(1:nmax),"bo-")
    plot((1:nmax),S,"rx-")
    legend(["Theory" "Experiment"])
    //
    MKL_Set_Num_Threads(backnumthreads)
endfunction

function demo_matmulMKLthreads()
    if ( ~isdef("MKL_Get_Max_Threads") ) then
        mprintf("Function MKL_Get_Max_Threads is not defined.\n");
        mprintf("Please install mkltools module:\n");
        mprintf("atomsInstall(""mkltools"")\n");
        mprintf("atomsLoad(""mkltools"")\n");
        return
    end
    scf();
    nmax = MKL_Get_Max_Threads()
    //[n,t,vart,S] = matmul_MKLthreads ( 0.5 , 30 , 1.2 , nmax );
    // Faster :
    [n,t,vart,S] = matmul_MKLthreads ( 0.5 , 5 , 1.2 , nmax );
endfunction

stacksize("max");
demo_matmulMKLthreads()
clear demo_matmulMKLthreads;



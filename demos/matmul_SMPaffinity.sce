// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function n = matmul_searchforsize ( timemin , nfact )
    // Returns the size of the matrix for which the A*B product is larger than timemin
    //
    // Example
    // stacksize("max");
    // n = matmul_searchforsize ( 1 , 1.2 )

    //
    // Let the size of the matrix grow dynamically

    backrand = rand("info")
    rand( "normal" );

    //
    // Make a loop over n
    n = 1;
    k = 1;
    while ( %t )
        A = rand(n,n);
        B = rand(n,n);
        tic();
        ierr = execstr("C = A * B","errcatch");
        t = toc();
        if ( ierr <> 0 ) then
            laerr = lasterror();
            msg = sprintf(gettext("%s: Error while running benchmark: %s."),..
            "matmul_searchforsize",laerr);
            warning(msg)
            break
        end
        if ( t > timemin ) then
            break
        end
        n = ceil(nfact * n);
    end
    rand(backrand)
endfunction

function c = specfun_combinerepeat ( x , k )
    //   Returns repeated combinations with replacement.
    //
    // Calling Sequence
    //   c = specfun_combinerepeat ( x , k )
    //
    // Parameters
    //   x : a m-by-n matrix, the matrix to produce combination from.
    //   k : a 1-by-1 matrix of floating point integers, the number of repeted combinations, must be greater than 1.
    //   c : a (k*m)-by-(n^k) matrix, same type as x
    //
    // Description
    //   Uses a fast algorithm to produce repeated combinations of x with itself.
    //   <itemizedlist>
    //   <listitem>If k=1, then returns x.</listitem>
    //   <listitem>If k=2, then returns combinations of x and x.</listitem>
    //   <listitem>If k=3, then returns combinations of x and x and x.</listitem>
    //   <listitem>etc...</listitem>
    //   </itemizedlist>
    //
    //   For performance reasons, the combinations are stored column-by-column, that
    //   is, the combination #k is in c(:,k), with k=1,2,...,n^k.
    //
    //   Can process x if x is double, strings boolean and integer (signed,unsigned,
    //   8-bits, 16-bits, 32-bits).
    //
    //   The algorithm makes use of the Kronecker product for good performances.
    //
    // Examples
    // // Compute repeated combinations of x:
    // x = [1 2 3];
    // specfun_combinerepeat ( x , 1 )
    // specfun_combinerepeat ( x , 2 )
    // specfun_combinerepeat ( x , 3 )
    // specfun_combinerepeat ( x , 4 )
    //
    // // Compare to specfun_combine
    // // Same as k=2
    // specfun_combine ( x , x )
    // // Same as k=3
    // specfun_combine ( x , x , x )
    // // Same as k=4
    // specfun_combine ( x , x , x , x )
    //
    // // Repeated combinations of booleans
    // computed = specfun_combinerepeat ( [%t %f] , 2 )
    // // Repeated combinations of strings
    // computed = specfun_combinerepeat ( ["A" "C" "T" "G"] , 2 )
    // // Repeated combinations of integers
    // computed = specfun_combinerepeat ( uint8(1:3) , 2 )
    //
    // // Compare combinerepeat, perms and subset
    // // Scilab/perms compute permutations without replacement
    // perms ( 1:3 )
    // // specfun_combinerepeat compute combinations with replacement
    // specfun_combinerepeat(1:3,3)'
    // // specfun_subset compute subsets with k elements
    // specfun_subset(1:3,3)
    //
    // Authors
    // Copyright (C) 2009 - Michael Baudin
    // Copyright (C) 2009-2010 - DIGITEO - Michael Baudin
    //

    [lhs,rhs]=argn()
    apifun_checkrhs ( "specfun_combinerepeat" , rhs , 2 )
    apifun_checklhs ( "specfun_combinerepeat" , lhs , 0:1 )
    //
    // Check Type
    apifun_checktype ( "specfun_combinerepeat" , x , "x" , 1 , ["constant" "string" "boolean" "int8" "uint8"  "int16" "uint16" "int32" "uint32"] )
    apifun_checktype ( "specfun_combinerepeat" , k , "k" , 2 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "specfun_combinerepeat" , k , "k" , 2 )
    //
    // Check content
    apifun_checkgreq ( "specfun_combinerepeat" , k , "k" , 2 , 1 )
    specfun_checkflint ( "specfun_combinerepeat" , k , "k" , 2 )
    // TODO : use apifun_checkflint when ready
    //
    // Proceed...
    c = x
    if ( k == 1 ) then
      return
    end
    for j = 2 : k
      c = specfun_combine( c , x )
    end
endfunction

function c = specfun_combine ( varargin )
    //   Returns the all the combinations of the given vectors.
    //
    // Calling Sequence
    //   c = specfun_combine ( a1 , a2 )
    //   c = specfun_combine ( a1 , a2 , a3 )
    //   c = specfun_combine ( a1 , a2 , a3 , ... )
    //
    // Parameters
    //   a1 : a m1-by-n1 matrix
    //   a2 : a m2-by-n2 matrix
    //   a3 : a m3-by-n3 matrix
    //   c : a m-by-n matrix, with m=m1+m2+m3+... and n=n1*n2*n3*...
    //
    // Description
    //   Uses a fast algorithm to produce combinations.
    //
    //   For performance reasons, the combinations are stored column-by-column, that
    //   is, the combination #k is in c(:,k), with k=1,2,...,n1*n2*n3*....
    //   The rows 1 to m1 contains elements from a1.
    //   The rows m1+1 to m1+m2 contains elements from a2.
    //   etc...
    //
    //   Can process matrices of doubles, strings, booleans and all integers (signed,unsigned,
    //   8-bits, 16-bits, 32-bits).
    //
    //   The algorithm makes use of the Kronecker product for good performances.
    //
    //   It works, for example, if:
    //   <itemizedlist>
    //   <listitem>all arguments are row vectors, with same number of entries,</listitem>
    //   <listitem>all arguments are column vectors, with same number of entries,</listitem>
    //   <listitem>all arguments are matrices, with same number of rows and columns,</listitem>
    //   <listitem>a row vector and a scalar,</listitem>
    //   <listitem>etc...</listitem>
    //   </itemizedlist>
    //
    // Examples
    // // Compute all combinations of x and y: vectors
    // x = [1 2 3];
    // y = [4 5 6];
    // specfun_combine ( x , y )
    // expected = [
    //   1 1 1 2 2 2 3 3 3
    //   4 5 6 4 5 6 4 5 6
    // ];
    //
    // // Compute all combinations of x and y and z: vectors
    // x = [1 2 3];
    // y = [4 5 6];
    // z = [7 8 9];
    // specfun_combine ( x , y , z )
    // expected = [
    //   1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 3 3 3 3 3 3 3 3 3
    //   4 4 4 5 5 5 6 6 6 4 4 4 5 5 5 6 6 6 4 4 4 5 5 5 6 6 6
    //   7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9
    // ];
    //
    // // Compute all combinations of x and y: matrices
    // x = [1 2;3 4];
    // y = [5 6;7 8];
    // specfun_combine ( x , y )
    // expected = [
    //   1 1 2 2
    //   3 3 4 4
    //   5 6 5 6
    //   7 8 7 8
    // ];
    //
    // // Combine random matrices of random sizes.
    // // Shows that matrices of any dimensions can be combined.
    // m = grand(1,2,"uin",1,5);
    // n = grand(1,2,"uin",1,5);
    // x = grand(m(1),n(1),"uin",1,m(1)*n(1));
    // y = grand(m(2),n(2),"uin",1,m(2)*n(2));
    // c = specfun_combine ( x , y );
    // and(size(c) == [m(1)+m(2) n(1)*n(2)])
    //
    // // Indirectly produce combinations of characters
    // k = specfun_combine ( 1:2 , 1:2 )
    // m1=["a" "b"];
    // m2=["c" "d"];
    // c = [m1(k(1,:));m2(k(2,:))]
    //
    // // Directly combine strings
    // x = ["a" "b" "c"];
    // y = ["d" "e"];
    // z = ["f" "g" "h"];
    // computed = specfun_combine ( x , y , z )
    //
    // // Produces combinations of booleans
    // c = specfun_combine ( [%t %f] , [%t %f] , [%t %f] )
    //
    // // Combine 2 DNA genes
    // c = specfun_combine ( ["A" "C" "G" "T"]  , ["A" "C" "G" "T"] )
    //
    // // Produces combinations of integers
    // c = specfun_combine(uint8(1:4),uint8(1:3))
    //
    // Authors
    // Copyright (C) 2009 - Michael Baudin
    // Copyright (C) 2009-2010 - DIGITEO - Michael Baudin
    //

    function c = combine2argsDouble ( X , Y )
        // Returns all combinations of the two row vectors X and Y
        cx=size(X,"c")
        cy=size(Y,"c")
        c=[X .*. ones(1,cy); ones(1,cx) .*. Y]
    endfunction

    function c = combine2argsOther ( X , Y )
      //
      // Compute the size of the args
      m(1) = size(X,"r")
      m(2) = size(Y,"r")
      n(1) = size(X,"c")
      n(2) = size(Y,"c")
      //
      // Combine the two first arguments
      //
      // Create and combine a matrix of indices
      matrix1 = matrix(1:m(1)*n(1),m(1),n(1))
      matrix2 = matrix(1:m(2)*n(2),m(2),n(2))
      cm = combine2argsDouble ( matrix1 , matrix2 )
      //
      // Get the actual strings to combine
      j = (1:n(1)*n(2))
      for i = 1:m(1)
        d = X(cm(i,j))
        c(i,j) = d(:)'
      end
      for i = 1:m(2)
        d = Y(cm(i+m(1),j))'
        c(i+m(1),j) = d(:)'
      end
    endfunction

    [lhs,rhs]=argn()
    apifun_checkrhs ( "specfun_combine" , rhs , 1:32 )
    apifun_checklhs ( "specfun_combine" , lhs , 0:1 )
    //
    // Check that all arguments are constants or strings
    for k = 1 : rhs
      apifun_checktype ( "specfun_combine" , varargin(k) , msprintf("a%d",k) , k , ["constant" "string" "boolean" "int8" "uint8"  "int16" "uint16" "int32" "uint32"] )
    end
    //
    // Check that all arguments have the same type
    type1 = typeof(varargin(1))
    for k = 2 : rhs
      apifun_checktype ( "specfun_combine" , varargin(k) , msprintf("a%d",k) , k , type1 )
    end
    //
    if ( rhs == 1 ) then
        c = varargin(1)
        return
    end
    if ( type1=="constant" ) then
      //
      // Combine doubles
      c = combine2argsDouble ( varargin(1) , varargin(2) )
      for i = 3:rhs
        c = combine2argsDouble ( c , varargin(i) )
      end
    else
      //
      // Combine strings
      c = combine2argsOther ( varargin(1) , varargin(2) )
      for i = 3:rhs
        c = combine2argsOther ( c , varargin(i) )
      end
    end
endfunction


function [n,t,vart] = matmul_SMPaffinity ( timemin , kmax , nfact , affinitymat )
    // Analyse performance of matmul w.r.t. processor affinity.
    //
    // Calling Sequence
    //   [n,t,mflops,vart,varm] = matmul_SMPaffinity ( timemin , kmax , nfact , affinitymat )
    //
    // Parameters
    //   timemin : a 1-by-1 matrix of doubles, the minimum time (seconds) for one run
    //   kmax : a 1-by-1 matrix of floating point integers, the number of runs for each affinity.
    //   nfact : a 1-by-1 matrix of doubles, must be greater than 1, the update factor of the matrix size computation.
    //   affinitymat : a na-by-nc matrix of booleans, the row-by-row affinity matrix, where na is the number of affinities and nc is the number of cores. The row vector affinitymat(i,1:nc) is the affinity vector for affinity #i, with i=1,2,...,na.
    //   n : a 1-by-1 matrix of floating point integers, the matrix size used in matrix multiply..
    //   t : a na-by-kmax matrix of doubles, the execution times. The variable t(i,k) contains the times for the affinity #i, with i=1,2,...,na and k=1,2,...,kmax.
    //   vart : a na-by-1 matrix of doubles, the relative variability in time (in percent). vart(i) is the relative variability of the elapsed time with affinity #i, with i=1,2,...,na.
    //
    // Description
    //   Analyse the sensitivity of the performance of matmul depending on the affinity of the cores.
    //
    // First, compute a matrix size n.
    // The matrix size is determined automatically from timemin and nfact.
    // Ideally, the matrix size n is the smallest n so that the time for one run is larger than timemin.
    // The matrix size n is updated from n = n * nfact.
    //
    // The main algorithm is the following.
    // Perform a loop on the affinities, from i = 1,2, ..., na.
    // For a particular affinity, perform kmax matrix multiply with n-by-n matrices.
    //
    // This function makes use of the specfun_combinerepeat, from the specfun module.
    //
    // Example
    // scf();
    // curraff = SMP_affinity ();
    // nc = SMP_getCpusCount();
    // na = 4;
    // affinitymat = (zeros(na,nc) == zeros(na,nc));
    // affinitymat(1:4,1) = [%t %t %f %f]';
    // affinitymat(1:4,2) = [%t %f %t %f]';
    // [n,t,vart] = matmul_SMPaffinity ( 0.5 , 30 , 1.2 , affinitymat );
    //
    // Authors
    // Copyright (C) 2010 - DIGITEO - Michael Baudin


    function C = mymatmul ( A , B )
        C = A * B;
    endfunction

    // Backup current affinity
    backaff = SMP_affinity ()
    //
    xtitle("Performance Variability of Matrix-Matrix Product","Affinity","Time (s)")
    mprintf("Minimum t = %f\n",timemin)
    mprintf("Searching for a matrix size...\n")
    n = matmul_searchforsize ( timemin , nfact )
    mprintf("Matrix size n=%d\n",n)
    [na,nc] = size(affinitymat)
    A = rand(n,n);
    B = rand(n,n);
    colknt = 1
    markknt = 1
    for i = 1 : na
        SMP_affinity(affinitymat(i,:))
        [ti,msg] = scibench_benchfun ( mymatmul , "mymatmul" , list(A,B) , 0 , kmax , %f )
        Tmin = min(ti)
        Tmax = max(ti)
        Tav = mean(ti)
        vart(i) = (Tmax-Tmin)/max([Tmax %eps]) * 100
        t(i,:) = ti(:)'
        plot(ones(kmax,1)*i,ti,"bo")
        //
        // Change the color
        h=gce()
        h.children.mark_foreground = colknt
        h.children.mark_style = markknt
        colknt = modulo(colknt + 1,10)
        markknt = modulo(markknt + 1,14)
        if ( colknt == 8 ) then
          // This is blank: skip
          colknt = colknt + 1
        end
        mprintf("Affinity #%d/%d: %s, Mean=%.2f, Tmin=%.2f, Tmax=%.2f, Relative variability: %.2f %%\n",...
          i,na,sci2exp(affinitymat(i,:)),Tav,Tmin,Tmax,vart(i))
    end
    //
    // Update the data bounds.
    // Leave room for the legend
    h = gcf()
    h.children.data_bounds(1,1) = 0
    h.children.data_bounds(2,1)=round(na*1.4)
    //
    // Update the legends
    legendmat = []
    for i = 1 : na
      legendmat(i) = sci2exp(affinitymat(i,:))
    end
    legend(legendmat)
    //
    SMP_affinity (backaff)
endfunction

function demo_matmulSMPaffinity ()
    if ( ~isdef("SMP_getCpusCount") ) then
        mprintf("Function SMP_getCpusCount is not defined.\n");
        mprintf("Please install SMP module:\n");
        mprintf("atomsInstall(""SMP"")\n");
        mprintf("atomsLoad(""SMP"")\n");
        return
    end
    scf()
    nc = SMP_getCpusCount()
    affinitymat = specfun_combinerepeat([%t %f],nc)'
    // Remove the experiment where all affinities are false
    k = find(and(~affinitymat,"c"))
    affinitymat(k,:) = []
    //affcomb = matmul_SMPaffinity ( 0.5 , 30 , 1.2 , affinitymat )
    // Faster :
    affcomb = matmul_SMPaffinity ( 0.1 , 5 , 1.2 , affinitymat )
endfunction
demo_matmulSMPaffinity ()
clear demo_matmulSMPaffinity;



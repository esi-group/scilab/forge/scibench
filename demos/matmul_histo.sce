// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function n = matmul_searchforsize ( timemin , nfact )
    // Returns the size of the matrix for which the A*B product is larger than timemin
    //
    // Example
    // stacksize("max");
    // n = matmul_searchforsize ( 1 , 1.2 )

    //
    // Let the size of the matrix grow dynamically

    backrand = rand("info")
    rand( "normal" );

    //
    // Make a loop over n
    n = 1;
    k = 1;
    while ( %t )
        A = rand(n,n);
        B = rand(n,n);
        tic();
        ierr = execstr("C = A * B","errcatch");
        t = toc();
        if ( ierr <> 0 ) then
            laerr = lasterror();
            msg = sprintf(gettext("%s: Error while running benchmark: %s."),..
              "matmul_searchforsize",laerr);
            warning(msg)
            break
        end
        if ( t > timemin ) then
            break
        end
        n = ceil(nfact * n);
    end
    rand(backrand)
endfunction

function [n,t,mflops,vart,varm] = matmul_histogram ( timemin , kmax , nfact )
    // Analyse the variability of the execution times required by matmul.
    //
    // Calling Sequence
    //   [n,t,mflops] = matmul_histogram ( timemin , kmax , nfact )
    //
    // Parameters
    //   timemin : a 1-by-1 matrix of doubles, the minimum time (seconds) for one run
    //   kmax : a 1-by-1 matrix of floating point integers, the number of runs
    //   nfact : a 1-by-1 matrix of doubles, must be greater than 1, the update factor of the matrix size computation.
    //   n : a 1-by-1 matrix of floating point integers, the matrix size
    //   t : a kmax-by-1 matrix of doubles, the execution times
    //   mflops : a kmax-by-1 matrix of doubles, the megaflops
    //   vart : a 1-by-1 matrix of doubles, the relative variability in time (in percent)
    //   varm : a 1-by-1 matrix of doubles, the relative variability in mflops (in percent)
    //
    // Description
    // Performs the matrix-matrix product kmax times, up to totaltime seconds.
    // This function may be used to see if the performances can be reproduced.
    //
    // The matrix size is determined automatically from timemin and nfact.
    // Ideally, the matrix size n is the smallest n so that the time for one run is larger than timemin.
    // In practice, the matrix size n is updated from n = n * nfact.
    //
    // Example
    // scf();
    // [n,t,mflops] = matmul_histogram ( 0.5 , 30 , 1.2 );
    //
    // Authors
    // Copyright (C) 2010 - DIGITEO - Michael Baudin


    mprintf("Minimum t = %f\n",timemin)
    // Get a reasonable matrix size
    mprintf("Searching for a matrix size...\n")
    n = matmul_searchforsize ( timemin , nfact );
    mprintf("Matrix size n=%d\n",n)
    for k = 1 : kmax
        A = rand(n,n);
        B = rand(n,n);
        tic();
        C = A * B;
        t(k) = toc();
        mflops(k) = floor(2*n^3/t(k)/1.e6);
        mprintf("Run #%d/%d: t = %f, mflops=%d\n",k,kmax,t(k),mflops(k))
    end
    subplot(1,2,1);
    histplot(10,t)
    // Compute the time variability in %
    vart = (max(t) - min(t))/max(t)*100
    // Compute the mflops variability in %
    varm = (max(mflops) - min(mflops))/max(mflops)*100
    thetitle = msprintf("Matrix-Matrix product - n=%d, Relative Variability=%.1f%%",n,vart)
    xtitle(thetitle,"Time (s)","Number of runs")
    subplot(1,2,2);
    histplot(10,mflops)
    thetitle = msprintf("Matrix-Matrix product - n=%d, Relative Variability=%.1f%%",n,varm)
    xtitle(thetitle,"Megaflops","Number of runs")
endfunction


stacksize("max");
scf();
[n,t,mflops] = matmul_histogram ( 0.5 , 30 , 1.2 );
clear matmul_histogram;



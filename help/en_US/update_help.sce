// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and
// creating them again from the .sci with help_from_sci.


//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);
//
// Generate the library help
mprintf("Updating scibench\n");
helpdir = fullfile(cwd);
funmat = [
  "scibench_getsettings"
  "scibench_benchfun"
  "scibench_dynbenchfun"
  "scibench_matmul"
  "scibench_backslash"
  "scibench_latticeboltz"
  "scibench_cholesky"
  "scibench_poisson"
  "scibench_poissonAu"
  "scibench_poissonA"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "scibench";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );



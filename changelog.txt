changelog of the Scilab Benchmark Toolbox

scibench (not released yet)
 * Added Poisson demo to the demo gateway script.

scibench (v0.6)
 * Fixed ticket 430: Fixed the overview page:
   scibench_backslash calling sequence changed.
 * Fixed ticket 431: Demos/Scibench/cholesky generates an error.
   The demos has been removed.
 * Added 2 missing help pages.
 * Fixed description of scibench_poisson.

 scibench (0.5)
 * Added two demos to measure the performance of the Cholesky decomposition.
 * Reduced the default timemax from 1 to 0.1 to have faster unit tests and demos.
 * Used assert module for unit tests.
 * Used helptbx for help pages.
 * Removed automatically generated demos.
 * Updated sci_benchfun so that the function now returns the
   time (backward compatibility lost).
   This ensures that pre-processing steps can be separated from
   the calculation step, within the callback.
 * Updated sci_dynbenchfun so that the function now returns the
   time (backward compatibility lost).
 * Fixed title in scibench_matmul.
 * Fixed title in scibench_backslash.
 * Created scibench_cholesky.
 * Added Poisson with sparse backslash.
 * Created separate functions poissonA and poissonAu.
 * Created dynamic benchmark for 2D Poisson.

scibench (0.4)
 * Added forgotten help page for dynbenchfun.

scibench (0.3)
 * Fixed bug in benchfun: verbose = %f produced error for msg
   http://forge.scilab.org/index.php/p/scibench/issues/166/
 * Fixed bug in benchfun: used timer instead of tic/toc.
 * Fixed bug in %BENCHENV_string.sci: variable env does not exist.
 * Created scibench_dynbenchfun to bench size increase.
 * Ported getsettings on Linux.
 * Created a benchmark to check sensitivity to MKL threads.
 * scibench_benchfun: provided default values of input arguments (backward compatibility lost).
   The function is now the first argument.
 * scibench_dynbenchfun: provided default values of input arguments (backward compatibility lost).
   The function is now the first argument.
 * Updated benchfun to manage extra parameters properly.
   Updated to manage an output function.
 * Updated backslash to manage default parameters, and rely on dynbenchfun.
 * Updated matmul to manage default parameters, and rely on dynbenchfun.

scibench (0.2)
    * Used apifun.
    * Updated help pages.
    * Updated unit tests.

scibench (0.1)
    * Initial version
    * Removed h from backslash and matmul: should be manage outside the function.

